﻿
// read only observables
//function AppViewModel() {
//    this.firstName = ko.observable('bob');
//    this.lastname = ko.observable('smith');
//    this.fullName = ko.computed(function () {
//        return this.firstName() + " " + this.lastname();
//    },this);
//}

// writable observables

function AppViewModel() {
    this.firstName = ko.observable('Planet');
    this.lastName = ko.observable('Earth');
    this.fullName = ko.computed({
        read: function () { return this.firstName() + " " + this.lastName(); },
        write: function (value) {
            var lastSpacePos = value.lastIndexOf(" ");
            if (lastSpacePos > 0)
            {
                this.firstName(value.substring(0, lastSpacePos));
                this.lastName(value.substring(lastSpacePos+1))
            }
        },
        owner:this
        
    });
}


function AppViewModel1() {
    this.price = ko.observable(25.99);
    this.formattedPrice = ko.computed({
        read: function () {
            return '$' + this.price().toFixed(2);
        },
        write: function (value) {
            value = parseFloat(value.replace(/[^\.\d]/g, ""));
            this.price(isNaN(value) ? 0 : value);
        },
        owner:this
    });
}