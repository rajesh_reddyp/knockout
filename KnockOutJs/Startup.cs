﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KnockOutJs.Startup))]
namespace KnockOutJs
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
